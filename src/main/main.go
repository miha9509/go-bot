package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

func main() {
	config, err := getConfig()
	if err != nil {
		log.Println("config error: ", err.Error())
	}
	botURL := config.API + config.Token
	offset := 0

	for {
		updates, err := getUpdates(botURL, offset)
		if err != nil {
			log.Println("smth went wrong: ", err.Error())
		}
		for _, update := range updates {
			err = respond(botURL, update)
			offset = update.UpdateID + 1
		}
		fmt.Println(updates)
	}
}

// Запрос обновлений
func getUpdates(botURL string, offset int) ([]Update, error) {
	resp, err := http.Get(botURL + "/getUpdates" + "?offset=" + strconv.Itoa(offset))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	var restResponse RestResponse
	err = json.Unmarshal(body, &restResponse)
	if err != nil {
		return nil, err
	}
	return restResponse.Result, nil
}

func respond(botURL string, update Update) error {
	botMessage := BotMessage{
		ChatID: update.Message.Chat.ChatID,
		Text:   update.Message.Text,
	}
	buf, err := json.Marshal(botMessage)
	if err != nil {
		return err
	}
	_, err = http.Post(botURL+"/sendMessage", "application/json", bytes.NewBuffer(buf))
	if err != nil {
		return err
	}
	return nil
}

func getConfig() (*Config, error) {
	// PathConfig Путь к конфиг файлу.
	pathConfig := "../../etc/config.json"
	// Получение конфига.
	file, err := os.Open(pathConfig)
	if err != nil {
		return nil, err
	}
	config := new(Config)
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	file.Close()
	if err != nil {
		return nil, err
	}
	return config, nil
}
